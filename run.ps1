# Step 1: Run Hugo build
Write-Host "Running Hugo build..."
hugo

# Step 3: Run JavaScript Puppeteer script
Write-Host "Running JavaScript Puppeteer script..."
Start-Process node -ArgumentList "generate-pdf.js"

# Step 2: Start Hugo serve in the background without opening a new window
Write-Host "Starting Hugo server..."
hugo serve
