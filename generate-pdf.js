const puppeteer = require('puppeteer');
(async () => {
    // Get the URL from command-line arguments
    const pageUrl = process.argv[2];

    if (!pageUrl) {
        console.error("Usage: node generate-pdf.js <URL>");
        process.exit(1);
    }
    const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'],devtools: true});
    const page = await browser.newPage();

    await page.goto(pageUrl, { waitUntil: 'networkidle2' });

    debugger;
    await page.pdf({
        path: 'public/Marcher Florian.pdf',  // Save inside 'public' so it's available for download
        format: 'A4',
        printBackground: true,
    });

 //   await browser.close();
})();
